FROM ubuntu:latest
LABEL mantainer="maximiliano.morales@istea.com.ar"
LABEL version="1.0"
LABEL description="TP Dockerfile"

ARG DEBIAN_FRONTEND=noninteractive

# Instalación de paquetes
RUN apt update && apt install -y apache2 mysql-server php libapache2-mod-php php-mysql && apt clean && apt autoremove


ENV APPSERVERNAME websiteprueba.com
ENV APPSERVERALIAS websiteprueba.com
ENV MYSQL_USER dbuser
ENV MYSQL_USER_PASSWORD 1234
ENV MYSQL_DB_NAME usuarios


# Crea el directorio 'app' en el directorio raiz.
RUN mkdir -p /app
# Copia el fichero de vHost al directorio 'sites-available'.
COPY default.conf /etc/apache2/sites-available
RUN rm -rf /var/www/html/*
COPY pagina/* /var/www/html/

# Copia todo el contenido de '/apache2' a 'app' a modo de backup.
RUN cp -r /etc/apache2 /app/apache2
# De haber un punto de montaje sobre el directorio '/apache2' el mismo se encontrará vacio.
# Para salvar esto, se crean las instrucciones correspondientes en el fichero 'entrypoint.sh' 
# para que copie el contenido del directorio '/app'.


#VOLUME ["/var/www/html", "/var/lib/mysql", "/etc/apache2"]


 
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
