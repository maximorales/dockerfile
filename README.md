*** Pasos para deploy de aplicación web con MySQL ***

1- Construcción de la imagen de Docker.

$ docker build -t registrodeusuarios .


2- Instanciamiento del contenedor.

$ docker run -d --name clientes \
          -e APPSERVERNAME=usuarios.com \
          -e APPSERVERALIAS=www.usuarios.com \
          -e MYSQL_USER=maximiliano \
          -e MYSQL_USER_PASSWORD=987654 \
          -e MYSQL_DB_NAME=basedeusuarios \
          -p 8080:80 \
          registrodeusuarios


3- Agregar en el archivo local de hosts la ip de la maquina que aloja el contenedor, junto con NAME y ALIAS.

<HOST IPADDRESS>    usuarios.com
<HOST IPADDRESS>    www.usuarios.com


4- Acceder a la aplicación web a través de un navegador

http://www.usuarios.com:8080


5- Para iniciar sesión (en primera instancia) utilizar las siguientes credenciales.

Usuario: jperez

Contraseña: abcd1234


6- Registrar un usuario nuevo a través del botón "Registrar".

7- Iniciar sesión con el usuario creado en el paso anterior.














