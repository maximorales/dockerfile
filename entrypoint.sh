#!/bin/bash
set -e

# Evalúa si el directorio '/etc/apache2' está vacío.
# Si el directorio está vacío, se recupera el contenido desde el directorio '/app'.


if [ -z "$(ls -A /etc/apache2)" ]; then

	cp -r /app/apache2/* /etc/apache2 

fi


# Modifica los valores 'ServerName' y 'ServerAlias' del fichero 'default.conf'.

sed -i 's/APPSERVERNAME/'"$APPSERVERNAME"'/' /app/apache2/sites-available/default.conf
sed -i 's/APPSERVERALIAS/'"$APPSERVERALIAS"'/' /app/apache2/sites-available/default.conf
cp /app/apache2/sites-available/default.conf /etc/apache2/sites-available/default.conf


# Activa el sitio.

a2ensite default.conf




# BASES DE DATOS

# Inicia el servicio mysql.

/etc/init.d/mysql start

# Demora cinco segundos luego del inicio del servicio para aplicar las siguientes instrucciones.

sleep 5

# Crea un usuario para la base de datos y establece la contraseña. Se crea un archivo testigo.

if [ ! -f /app/mysql.configured ]; then
	
	if [ $MYSQL_USER_PASSWORD == "1234" ]; then
		
		RPASS=$((1000 + $RANDOM %30000))
		
		mysql -u root -e "CREATE USER '$MYSQL_USER'@'localhost' IDENTIFIED BY '$RPASS';"
		touch /app/mysql.configured

	else
		mysql -u root -e "CREATE USER '$MYSQL_USER'@'LOCALHOST' IDENTIFIED BY '$MYSQL_USER_PASSWORD';"
		touch /app/mysql.configured
	fi
fi

# Crea la base de datos con archivo testigo para evitar errores al stopear y restartear el contenedor.

if [ ! -f /app/database.configured ]; then

	mysql -u root -e "CREATE DATABASE $MYSQL_DB_NAME;"
	mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO '$MYSQL_USER'@'localhost';"
	touch /app/database.configured

fi


# Crea una tabla de muestra

if [ ! -f /app/table.configured ]; then
	
	mysql -u root -e "USE $MYSQL_DB_NAME;
       	CREATE TABLE clientes(username varchar(20) not null,
		              nombre varchar(30),
			      correo varchar(50),
		              contra varchar(20));
        INSERT INTO clientes values('jperez', 'Juan Perez', 'juan.perez@istea.com', 'abcd1234');"
        touch /app/table.configured
fi

#Cambia los valores de conexión a la base de datos en los archivos de la app web.

sed -i 's/dbuser/'"$MYSQL_USER"'/' /var/www/html/conexion.php
sed -i 's/dbuser/'"$MYSQL_USER"'/' /var/www/html/registro.php
sed -i 's/dbpassword/'"$MYSQL_USER_PASSWORD"'/' /var/www/html/conexion.php
sed -i 's/dbpassword/'"$MYSQL_USER_PASSWORD"'/' /var/www/html/registro.php
sed -i 's/dbname/'"$MYSQL_DB_NAME"'/' /var/www/html/conexion.php
sed -i 's/dbname/'"$MYSQL_DB_NAME"'/' /var/www/html/registro.php


# Inicio de servicios

apachectl -D FOREGROUND




exec "$@"
